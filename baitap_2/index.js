function tienDien() {
    var name = document.getElementById("name").value;
    var soKw = document.getElementById("so-kw").value * 1;
    var giaTienDien;
  
    if (soKw <= 50) {
      giaTienDien = soKw * 500;
    } else if (soKw <= 100) {
      giaTienDien = 50 * 500 + (soKw - 50) * 650;
    } else if (soKw <= 200) {
      giaTienDien = 50 * 500 + 50 * 650 + (soKw - 100) * 850;
    } else if (soKw <= 350) {
      giaTienDien = 50 * 500 + 50 * 650 + 100 * 850 + (soKw - 200) * 1100;
    } else {
      giaTienDien =
        50 * 500 + 50 * 650 + 100 * 850 + 150 * 1100 + (soKw - 350) * 1300;
    }
  
    document.getElementById("ket-qua").innerHTML = `<h1>
    Họ tên: ${name}. Tiền điện: ${giaTienDien.toLocaleString()}
    </h1>`;
  }
  