function ketQua() {
    var diemChuan = document.getElementById("diem-chuan").value * 1;
    var khuVuc = document.getElementById("khu-vuc").value;
    var diemKhuVuc;
    var doiTuong = document.getElementById("doi-tuong").value;
    var diemDoiTuong;
    var mon1 = document.getElementById("mon-1").value * 1;
    var mon2 = document.getElementById("mon-2").value * 1;
    var mon3 = document.getElementById("mon-3").value * 1;
  
    if (khuVuc == "A") {
      diemKhuVuc = 2;
    } else if (khuVuc == "B") {
      diemKhuVuc = 1;
    } else if (khuVuc == "C") {
      diemKhuVuc = 0.5;
    } else {
      diemKhuVuc = 0;
    }
  
    if (doiTuong == "1") {
      diemDoiTuong = 2.5;
    } else if (khuVuc == "2") {
      diemDoiTuong = 1.5;
    } else if (khuVuc == "3") {
      diemDoiTuong = 1;
    } else {
      diemDoiTuong = 0;
    }
  
    var tongDiem = mon1 + mon2 + mon3 + diemKhuVuc + diemDoiTuong;
  
    if (mon1 == 0 || mon2 == 0 || mon3 == 0 || tongDiem < diemChuan) {
      document.getElementById(
        "ket-qua"
      ).innerHTML = `<h1> Bạn đã rớt. Tổng điểm là: ${tongDiem}</h1>`;
    } else {
      document.getElementById(
        "ket-qua"
      ).innerHTML = `<h1> Bạn đã đậu. Tổng điểm là: ${tongDiem} </h1>`;
    }
  }
  